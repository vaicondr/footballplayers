#!/usr/bin/env python3
import sys
import copy
import math

CONST_SOURCE = "S"
CONST_PLAYER = "P"
CONST_COORDS = "C"
CONST_SINK = "T"


def load_data(filename):
    with open(filename) as f:
        positions = []
        for i, line in enumerate(f):
            numbers = line.split(" ")
            nums = list(map(int, numbers))
            if i == 0:
                number_of_players = nums[0]
                number_of_frames = nums[1]
            else:
                positions.append(nums)
    return number_of_players, number_of_frames, positions


def get_initial_feasible_flow(graph):
    ford_fulkerson(transformed_graph, CONST_TRANSFORMED_SOURCE, CONST_TRANSFORMED_SINK)
    feasibility = remove_special_edges_and_check_feasiblity_of_initial_flow(graph, transformed_graph)
    return feasibility

def ford_fulkerson(graph, source, target):
    while 1:
        path = find_augmenting_path(graph, source, target)
        if path == -1:
            return
        augment_path(graph, path)


def find_augmenting_path(graph, source, target):
    queue = [(source, ([source], math.inf))]
    labels = labeling(graph, source, target)

    if labels == -1:
        return -1
    while queue:
        (vertex, (path, capacity)) = queue.pop(0)
        for forward in graph.get_forward_arcs_from(vertex):
            edge_target = get_target_of_edge(forward)
            if edge_target not in path and labels[edge_target]:
                if get_flow_of_edge(forward) < get_upper_bound_of_edge(forward):
                    potential_capacity = get_upper_bound_of_edge(forward) - get_flow_of_edge(forward)
                    if potential_capacity == 0:
                        continue
                    if edge_target == target:
                        return path + [edge_target], min(potential_capacity, capacity)
                    else:
                        queue.append((edge_target, (path + [edge_target], min(potential_capacity, capacity))))
        for backward in graph.get_backward_arcs_from(vertex):
            edge_target = get_source_of_edge(backward)
            if edge_target not in path and labels[edge_target]:
                if get_flow_of_edge(backward) > get_lower_bound_of_edge(backward):
                    potential_capacity = get_flow_of_edge(backward) - get_lower_bound_of_edge(backward)
                    if potential_capacity == 0:
                        continue
                    queue.append((edge_target, (path + [edge_target], min(potential_capacity, capacity))))
    return -1

def augment_path(graph, path):
    capacity = path[1]
    for i in range(len(path[0]) - 1):
        source = path[0][i]
        target = path[0][i+1]
        original_edge = graph.get_edge(source, target)
        if original_edge is not 0:
            graph.add_edge(Edge(source, target, original_edge.get_lower_bound(),
                                       get_flow_of_edge(original_edge) + capacity,
                                       get_upper_bound_of_edge(original_edge)))
        else:
            original_edge = graph.get_edge(target, source)
            graph.add_edge(create_edge(target, source, get_lower_bound_of_edge(original_edge),
                                       get_flow_of_edge(original_edge) - capacity,
                                       get_upper_bound_of_edge(original_edge)))


def build_residual_graph(graph, flow):
    residual_graph = Graph()
    for edge in graph.get_all_edges():
        forward_edge = create_edge(edge.get_source, edge.get_target, edge.get_upper_bound() - edge.get_flow(), edge.get_cost())
        reversed_edge = create_edge(edge.get_target, edge.get_source, edge.get_flow() - edge.get_flow(), -edge.get_cost() )
        residual_graph.append(forward_edge)
        residual_graph.append(reversed_edge)
    return residual_graph


def find_negative_cost_cycle(residual_graph, number_of_player):
    #remove all edges where u_f(e) = 0
    # connect source to every node with cf(e) = 0
    # use bellman ford to detect negative cycle
    l = [] * number_of_players
    for i in range(number_of_players):
        l[i] = math.inf
    l[CONST_SOURCE] = 0

    for i in range(number_of_players - 1):
        for edge in residual_graph.get_all_edges():
            if(l[edge.get_target()] > l[edge.get_source()] + cost_function(edge.get_source)):
                    l[edge.get_target] = l[edge.get_source] + cost_function(edge.get_source)

    for edge in residual_graph.get_all_edges():
        if (l[edge.get_target()] > l[edge.get_source()] + cost_function(edge.get_source)):
            return 1

def cost_function(x1, x2, y1, y2):
    return math.sqrt((x1 - x2)**2 + (y1 - y2)**2)

def cycle_canceling(graph):
    find_feasible_flow(graph)
    while(1):
        residual = build_residual_graph(graph, flow)
        cycle = find_negative_cost_cycle(residual,)
        if cycle = -1:
            return
        augment_path(graph)


class Graph:
    def __init__(self, number_of_players, positions):
        self.number_of_elements = number_of_players + 2
        self.matrix = [[Edge for i in range(self.number_of_elements)] for j in range(self.number_of_elements)]

    def connect_source_to_players(self, number_of_players):
        for i in range(number_of_players):
            self.add_edge(Edge(CONST_SOURCE,CONST_PLAYER + i, 0, 0, 1))

    def connect_players_to_positions(self):
        for i in range(number_of_players):
            for j in range(number_of_players):
                self.add_edge(Edge(CONST_PLAYER + i, CONST_COORDS + j, 0, 0, 1))

    def connect_positions_to_sink(self):
        for i in range(number_of_players):
            self.add_edge(Edge(CONST_COORDS + i,CONST_SINK, 0, 0, 1))

    def get_forward_arcs_from(self, vertex):
        edges = []
        for edge in self.matrix[self.get_element_index(vertex)]:
            if edge is not 0:
                edges.append(edge)
        return edges

    def get_backward_arcs_from(self, vertex):
        edges = []
        for i in range(self.number_of_elements):
            edge = self.matrix[i][self.get_element_index(vertex)]
            if edge is not 0:
                edges.append(edge)
        return edges

    def add_edge(self, edge):
        self.matrix[self.get_element_index(edge.get_source())][self.get_element_index((edge.get_target()))] = edge

    def get_edge(self, source, target):
        return self.matrix[self.get_element_index(source)][self.get_element_index(target)]

    def get_all_edges(self):
        edges = []
        for i in range(self.number_of_elements):
            for edge in self.matrix[i]:
                if edge is not 0:
                    edges.append(edge)
        return edges

    def get_element_index(self, element):
        if element.startswith(CONST_PLAYER):
            return int(element[1:])
        elif element.startswith(CONST_COORDS):
            return number_of_players + int(element[1:])
        else:
            if element.startswith(CONST_SOURCE):
                return self.number_of_elements - 2
            # elif element.startswith(CONST_TRANSFORMED_SINK):
            #     return number_of_customers + number_of_products + 3
            # elif element.startswith(CONST_SOURCE):
            #     return number_of_customers + number_of_products
            elif element.startswith(CONST_SINK):
                return self.number_of_elements - 1

class Edge:
    def __init__(self, source, target, lower_bound, flow, upper_bound ):
        self.source = source
        self.target = target
        self.flow = flow
        self.upper_bound = upper_bound
        self.lower_bound = lower_bound

    def get_source(self):
        return self.source

    def get_target(self):
        return self.target

    def get_lower_bound(self):
        return self.lower_bound

    def get_upper_bound(self):
        return self.upper_bound

    def get_flow(self):
        return self.flow


input_file = sys.argv[1]
output_file = sys.argv[2]
number_of_players, number_of_frames, positions = load_data(input_file)
graph = Graph()
cycle_canceling(graph)


